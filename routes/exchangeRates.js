const express = require("express");
const axios = require("axios");
const cheerio = require("cheerio");
const url = "https://old.mongolbank.mn/dblistofficialdailyrate.aspx";
const urlUSA = "https://www.federalreserve.gov/releases/H10/current/";
const urlx = "https://www.x-rates.com/historical/?from=USD&amount=1&date=";
const moment = require("moment");
const router = express.Router();

router.get("/BankofMongolia", async function (req, res) {
    if (req.query.currency) {
        var currencies = req.query.currency.split('|');
        currencies = currencies.map(function (x) {
            return x.toUpperCase();
        });
    }
    axios
        .get(url)
        .then(response => {
            const html = response.data;
            const $ = cheerio.load(html);
            const dateBefore = $("#ContentPlaceHolder1_lblDate").text();
            var date = dateBefore.replace(/[^0-9.]/g, ".");
            date = date
                .replace(/[.]+/g, "-")
                .trim()
                .slice(0, -1);
            const dates = date.split('-');
            if (dates[1].length == 1) {
                dates[1] = "0" + dates[1];
            }
            if (dates[2].length == 1) {
                dates[2] = "0" + dates[2];
            }
            const fullDate = dates.join('.');
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var tomorrowDate = tomorrow.toISOString();
            tomorrowDate = tomorrowDate.substring(0, 10).replace(/-/g, '.');
            const usd = parseFloat($("#ContentPlaceHolder1_lblUSD").text().replace(/,/g, ''));
            const eur = parseFloat($("#ContentPlaceHolder1_lblEUR").text().replace(/,/g, ''));
            const jpy = parseFloat($("#ContentPlaceHolder1_lblJPY").text().replace(/,/g, ''));
            const chf = parseFloat($("#ContentPlaceHolder1_lblCHF").text().replace(/,/g, ''));
            const sek = parseFloat($("#ContentPlaceHolder1_lblSEK").text().replace(/,/g, ''));
            const gbp = parseFloat($("#ContentPlaceHolder1_lblGBP").text().replace(/,/g, ''));
            const bgn = parseFloat($("#ContentPlaceHolder1_lblBGN").text().replace(/,/g, ''));
            const huf = parseFloat($("#ContentPlaceHolder1_lblHUF").text().replace(/,/g, ''));
            const egp = parseFloat($("#ContentPlaceHolder1_lblEGP").text().replace(/,/g, ''));
            const inr = parseFloat($("#ContentPlaceHolder1_lblINR").text().replace(/,/g, ''));
            const hkd = parseFloat($("#ContentPlaceHolder1_lblHKD").text().replace(/,/g, ''));
            const rub = parseFloat($("#ContentPlaceHolder1_lblRUB").text().replace(/,/g, ''));
            const kzt = parseFloat($("#ContentPlaceHolder1_lblKZT").text().replace(/,/g, ''));
            const cny = parseFloat($("#ContentPlaceHolder1_lblCNY").text().replace(/,/g, ''));
            const krw = parseFloat($("#ContentPlaceHolder1_lblKRW").text().replace(/,/g, ''));
            const kpw = parseFloat($("#ContentPlaceHolder1_lblKPW").text().replace(/,/g, ''));
            const cad = parseFloat($("#ContentPlaceHolder1_lblCAD").text().replace(/,/g, ''));
            const aud = parseFloat($("#ContentPlaceHolder1_lblAUD").text().replace(/,/g, ''));
            const czk = parseFloat($("#ContentPlaceHolder1_lblCZK").text().replace(/,/g, ''));
            const twd = parseFloat($("#ContentPlaceHolder1_lblTWD").text().replace(/,/g, ''));
            const thb = parseFloat($("#ContentPlaceHolder1_lblTHB").text().replace(/,/g, ''));
            const idr = parseFloat($("#ContentPlaceHolder1_lblIDR").text().replace(/,/g, ''));
            const myr = parseFloat($("#ContentPlaceHolder1_lblMYR").text().replace(/,/g, ''));
            const sgd = parseFloat($("#ContentPlaceHolder1_lblSGD").text().replace(/,/g, ''));
            const aed = parseFloat($("#ContentPlaceHolder1_lblAED").text().replace(/,/g, ''));
            const kwd = parseFloat($("#ContentPlaceHolder1_lblKWD").text().replace(/,/g, ''));
            const dkk = parseFloat($("#ContentPlaceHolder1_lblDKK").text().replace(/,/g, ''));
            const pln = parseFloat($("#ContentPlaceHolder1_lblPLN").text().replace(/,/g, ''));
            const uah = parseFloat($("#ContentPlaceHolder1_lblUAH").text().replace(/,/g, ''));
            const nok = parseFloat($("#ContentPlaceHolder1_lblNOK").text().replace(/,/g, ''));
            const npr = parseFloat($("#ContentPlaceHolder1_lblNPR").text().replace(/,/g, ''));
            const zar = parseFloat($("#ContentPlaceHolder1_lblZAR").text().replace(/,/g, ''));
            const TRY = parseFloat($("#ContentPlaceHolder1_lblTRY").text().replace(/,/g, ''));
            const vnd = parseFloat($("#ContentPlaceHolder1_lblVND").text().replace(/,/g, ''));
            var responceJson = {
                DATES: {
                    RATEDATE: fullDate,
                    YEAR: dates[0],
                    MONTH: dates[1],
                    DAY: dates[2],
                    TOMORROW: tomorrowDate
                }
            };
            if (currencies) {
                var currenciesNumber = currencies.length;
                for (var i = 0; i < currenciesNumber; i++) {
                    var iteraion = currencies[i].toLowerCase();
                    responceJson[currencies[i]] = eval(iteraion);
                }
                res.status(200).json(responceJson);
            } else {
                res.status(200).json({
                    DATES: {
                        RATEDATE: fullDate,
                        YEAR: dates[0],
                        MONTH: dates[1],
                        DAY: dates[2],
                        TOMORROW: tomorrowDate
                    },
                    USD: usd,
                    EUR: eur,
                    JPY: jpy,
                    CHF: chf,
                    SEK: sek,
                    GBP: gbp,
                    BGN: bgn,
                    HUF: huf,
                    EGP: egp,
                    INR: inr,
                    HKD: hkd,
                    RUB: rub,
                    KZT: kzt,
                    CNY: cny,
                    KRW: krw,
                    KPW: kpw,
                    CAD: cad,
                    AUD: aud,
                    CZK: czk,
                    TWD: twd,
                    THB: thb,
                    IDR: idr,
                    MYR: myr,
                    SGD: sgd,
                    AED: aed,
                    KWD: kwd,
                    DKK: dkk,
                    PLN: pln,
                    UAH: uah,
                    NOK: nok,
                    NPR: npr,
                    ZAR: zar,
                    TRY: TRY,
                    VND: vnd
                });
            }
        })
        .catch(error => {
            console.log(error);
            res.status(400).json({
                message: "Алдаа гарсан тул та админд мэдэгдэнэ үү?"
            });
        });
});

router.get("/BankofUSA", async function (req, res) {
    let year = new Date().getFullYear();
    axios
        .get(urlUSA)
        .then(response => {
            const html = response.data;
            const $ = cheerio.load(html);
            let responseBody = {
                "exchangeRates": []
            };
            $('table.statistics > thead > tr > th').each(function (index, tr) {
                if (/[0-9]/.test($(this).text())) {
                    let dateText = $(this).text().replace(/ /g, '').split(".");
                    let month = "JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(dateText[0]) / 3 + 1;
                    if (month < 10) {
                        month = '0' + month;
                    }
                    responseBody.exchangeRates.push({
                        "date": year + '.' + month + '.' + dateText[1],
                        "rates": []
                    });
                }
            });
            $('table.statistics > tbody > tr').each(function (index, tr) {
                let header = $(this).find('th').text().replace(/[^A-Za-z']/g, '');
                if (header == "Memo") {
                    return false;
                }
                let i = 0;
                let currency;
                $(this).find('td').each(function (index, td) {
                    let value = $(this).text().replace(/ /g, '');
                    if (i == 0) {
                        currency = header + ' ' + value;
                        switch (header) {
                            case "AUSTRALIA":
                                currCode = "AUD";
                                break;
                            case "BRAZIL":
                                currCode = "BRL";
                                break;
                            case "CANADA":
                                currCode = "CAD";
                                break;
                            case "CHINAPR":
                                currCode = "CNY";
                                break;
                            case "DENMARK":
                                currCode = "DKK";
                                break;
                            case "EMUMEMBERS":
                                currCode = "EUR";
                                break;
                            case "HONGKONG":
                                currCode = "HKD";
                                break;
                            case "INDIA":
                                currCode = "INR";
                                break;
                            case "JAPAN":
                                currCode = "JPY";
                                break;
                            case "MALAYSIA":
                                currCode = "MYR";
                                break;
                            case "MEXICO":
                                currCode = "MXN";
                                break;
                            case "NEWZEALAND":
                                currCode = "NZD";
                                break;
                            case "NORWAY":
                                currCode = "NOK";
                                break;
                            case "SINGAPORE":
                                currCode = "SGD";
                                break;
                            case "SOUTHAFRICA":
                                currCode = "ZAR";
                                break;
                            case "SOUTHKOREA":
                                currCode = "KRW";
                                break;
                            case "SRILANKA":
                                currCode = "LKR";
                                break;
                            case "SWEDEN":
                                currCode = "SEK";
                                break;
                            case "SWITZERLAND":
                                currCode = "CHF";
                                break;
                            case "TAIWAN":
                                currCode = "TWD";
                                break;
                            case "THAILAND":
                                currCode = "THB";
                                break;
                            case "UNITEDKINGDOM":
                                currCode = "GBP";
                                break;
                            case "VENEZUELA":
                                currCode = "VEF";
                                break;
                            default:
                                currCode = "NOT DEFINED";
                        }
                    } else {
                        responseBody.exchangeRates[i - 1].rates.push({
                            "currencyName": currency,
                            "currencyCode": currCode,
                            "rate": value
                        });
                    }
                    i++;
                });
            });
            res.status(200).json(responseBody);
        }).catch(error => {
            console.log(error);
            res.status(400).json({
                message: "Алдаа гарсан тул та админд мэдэгдэнэ үү?"
            });
        });
});

router.get("/x-rates", async function (req, res) {
    let fullURL, tmr, today;
    let customDate = req.query.date;
    if (customDate) {
        fullURL = urlx + customDate;
        today = moment(customDate).format("YYYY-MM-DD");
        tmr = moment().add(1, "days").format("YYYY-MM-DD");
    } else {
        today = moment().format("YYYY-MM-DD");
        fullURL = urlx + today;
        tmr = moment().add(1, "days").format("YYYY-MM-DD");
    }

    let respData = {
        "date": today,
        "tomorrow": tmr
    };
    axios
        .get(fullURL)
        .then(response => {
            let html = response.data;
            let $ = cheerio.load(html);
            $('table.ratesTable > tbody > tr').each(function (index, tr) {
                let i = 0;
                let currency = "";
                let rate = 0;
                $(this).find('td').each(function (index, td) {
                    let value = $(this).text();
                    if (!/\d/.test(value)) {
                        switch (value) {
                            case "Australian Dollar":
                                currency = "AUD";
                                break;
                            case "Swiss Franc":
                                currency = "CHF";
                                break;
                            case "Chinese Yuan Renminbi":
                                currency = "CNY";
                                break;
                            case "Canadian Dollar":
                                currency = "CAN";
                                break;
                            case "Danish Krone":
                                currency = "DKK";
                                break;
                            case "Euro":
                                currency = "EUR";
                                break;
                            case "British Pound":
                                currency = "GBP";
                                break;
                            case "Japanese Yen":
                                currency = "JPY";
                                break;
                            case "New Zealand Dollar":
                                currency = "NZD";
                                break;
                            case "Norwegian Krone":
                                currency = "NOK";
                                break;
                            case "Polish Zloty":
                                currency = "PLN";
                                break;
                            case "Romanian New Leu":
                                currency = "RON";
                                break;
                            case "Russian Ruble":
                                currency = "RUB";
                                break;
                            case "Swedish Krona":
                                currency = "SEK";
                                break;
                            default:
                                currency = "UNK";
                        }
                    } else if (i == 2) {
                        return false;
                    } else if (currency != "UNK") {
                        rate = parseFloat(value);
                        respData = {
                            ...respData,
                            [currency]: rate
                        }
                    }
                    i++;
                });
            });
            res.status(200).json(respData);
        }).catch(error => {
            console.log(error);
            res.status(400).json({
                message: "Алдаа гарсан тул та админд мэдэгдэнэ үү?"
            });
        });
});
module.exports = router;