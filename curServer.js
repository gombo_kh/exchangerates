const express = require("express");
const app = express();
const rateRoutes = require("./routes/exchangeRates");
const dotenv = require("dotenv");
dotenv.config();
app.use(express.static("public"));
app.use(express.json());
app.use("/rates", rateRoutes);

const server = app.listen(
  process.env.PORT,
  console.log(`Server port ${process.env.PORT}`)
);

process.on("unhandledRejection", (err, promise) => {
  console.log(`Error unhandled : ${err.message}`);
  server.close(() => {
    process.exit(1);
  });
});